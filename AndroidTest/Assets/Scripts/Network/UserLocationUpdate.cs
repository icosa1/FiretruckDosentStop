﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UserLocationUpdate : MonoBehaviour
{
    static float UPDATE_TIME = 3f;
    float updateTime = UPDATE_TIME;

    enum UserType
    {
        General,
        Fire_truck
    };
    UserType userType = UserType.General;
    public Text id;
    string url;

    public static UserLocationUpdate instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    void Start()
    {
        StartCoroutine(LocationUpdate());
    }
    void Update()
    {
        if (updateTime >= 0)
        {
            updateTime = updateTime - Time.deltaTime;
        }
        else
        {
            updateTime = UPDATE_TIME;
            StartCoroutine(LocationUpdate());
        }
    }
    IEnumerator LocationUpdate() {
        if (PlayerPrefs.HasKey("ID"))
        {
            //start update
            if (userType == UserType.General)
            {
                //start
                url = "https://yfxesyklhi.execute-api.ap-northeast-2.amazonaws.com/update_general_user_location/update?User_ID=" + PlayerPrefs.GetString("ID")
                    + "&Latitude=" + LocationData.instance.lat + "&Longitude=" + LocationData.instance.lon;
            }
            else if (userType == UserType.Fire_truck)
            {
                //start
                url = "https://8qwhyk09b0.execute-api.ap-northeast-2.amazonaws.com/update_fire_truck_user_location/update?User_ID=" + PlayerPrefs.GetString("ID")
                    + "&Latitude=" + LocationData.instance.lat + "&Longitude=" + LocationData.instance.lon;
            }

            using (UnityWebRequest www = UnityWebRequest.Post(url, ""))
            {
                //업데이트시 사용
                www.method = "PATCH";

                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    Debug.Log("User Location upload complete!");
                }
            }

            //destination update
            if (userType == UserType.General)
            {
                //destination
                url = "https://mjg6gxy1s0.execute-api.ap-northeast-2.amazonaws.com/destination_general_user/destination?User_ID=" + PlayerPrefs.GetString("ID") +
                    "&D_Lat=" + LocationData.instance.lat2 + "&D_Lon=" + LocationData.instance.lon2;
            }

            using (UnityWebRequest www = UnityWebRequest.Post(url, ""))
            {
                //업데이트시 사용
                www.method = "PATCH";

                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    Debug.Log("User destination upload complete!");
                }
            }
        }
        yield return null;
    }
}
