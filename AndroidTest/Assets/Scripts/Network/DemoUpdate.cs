﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DemoUpdate : MonoBehaviour
{
    static float UPDATE_TIME = 1.5f;
    float updateTime = UPDATE_TIME;
    char[] delimiterChars = { ' ', ',', '[', ']', '\n' };

    enum UserType
    {
        General,
        Fire_truck
    };
    UserType userType = UserType.General;
    string id = "";
    string url;


    void Start()
    {
        StartCoroutine(LocationUpdate());
    }
    void Update()
    {
        if (updateTime >= 0)
        {
            updateTime = updateTime - Time.deltaTime;
        }
        else
        {
            updateTime = UPDATE_TIME;
            StartCoroutine(LocationUpdate());
        }
    }
    IEnumerator LocationUpdate() {
            //start update
            if (userType == UserType.General)
            {
                //start
                url = "https://fmtzdf6ms0.execute-api.ap-northeast-2.amazonaws.com/find_destination/find?User_ID=Normal_1";
            }
            else if (userType == UserType.Fire_truck)
            {
            }

            using (UnityWebRequest www = UnityWebRequest.Post(url, ""))
            {
                //업데이트시 사용
                www.method = "GET";

                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    //Debug.Log("Demo get complete!");
                    //Debug.Log(www.downloadHandler.text);
                    string lotation = www.downloadHandler.text;
                    string[] lot = lotation.Split(delimiterChars);
                    LocationData.instance.lat = Double.Parse(lot[1]);
                    LocationData.instance.lon = Double.Parse(lot[3]);
                    LocationData.instance.lat2 = Double.Parse(lot[5]);
                    LocationData.instance.lon2 = Double.Parse(lot[7]);
                }
            }

        yield return null;
    }
}
