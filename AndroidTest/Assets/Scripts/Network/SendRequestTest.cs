﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Net;
using System.IO;
using System.Text;

public class SendRequestTest : MonoBehaviour
{
    string url;
    string id;// = "wasitacatisaw";
    string password;
    public Text idtext;
    public Text passwordtext;

    public Textchanger changer;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("ID"))
        {
            gameObject.transform.parent.gameObject.SetActive(false);
        }
    }
    void Update()
    {
        id = idtext.text;
        password = passwordtext.text;
    }
    public void Send()
    {
        StartCoroutine(SendRequest());
    }
    IEnumerator SendRequest()
    {
        url = "https://km2fwew0y2.execute-api.ap-northeast-2.amazonaws.com/Login_General_User/login?User_ID=" + id + "&Password=" + password;

        HttpWebRequest request3 = (HttpWebRequest)WebRequest.Create(url);
        request3.Method = ("GET");
        HttpWebResponse response3 = (HttpWebResponse)request3.GetResponse();
        string status = response3.StatusCode.ToString();
        if (status == "OK")
        {
            Stream stream = response3.GetResponseStream();
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            string center_file = reader.ReadToEnd();
            if (string.Equals(center_file, "\"InvalidID\""))
            {
                StartCoroutine(RegisterRequest());
                PlayerPrefs.SetString("ID", id);
                PlayerPrefs.SetString("Password", password);
                PlayerPrefs.Save();
                changer.textchange("회원등록에 성공하였습니다.");
                // 부모 오브젝트(버튼 그룹) 꺼버리기
                gameObject.transform.parent.gameObject.SetActive(false);
            }
            else if (string.Equals(center_file, "\"True\""))
            {
                PlayerPrefs.SetString("ID", id);
                PlayerPrefs.SetString("Password", password);
                PlayerPrefs.Save();
                changer.textchange("로그인에 성공하였습니다.");
                // 부모 오브젝트(버튼 그룹) 꺼버리기
                gameObject.transform.parent.gameObject.SetActive(false);
            }
            else if (string.Equals(center_file, "\"WrongPassword\"")) {
                changer.textchange("잘못된 비밀번호입니다.");
            }
        }
        yield return null;
    }
    IEnumerator RegisterRequest() {
        url = "https://q7gmtadv3c.execute-api.ap-northeast-2.amazonaws.com/Register_General_User/register?User_ID=" + id + "&Latitude=37.43&Longitude=123.33"
            + "&Password=" + password;

        using (UnityWebRequest www = UnityWebRequest.Post(url, ""))
        {
            //업데이트시 사용
            //www.method = "PATCH";

            www.SetRequestHeader("Accept", "application/json");
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
    }
}