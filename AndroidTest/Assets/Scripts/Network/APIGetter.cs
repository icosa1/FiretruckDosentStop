﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class APIGetter : MonoBehaviour
{
    //for test text
    public Text apitext;
    public Alarm alarm;
    string apistring;

    enum State {
        Normal,
        Emergency
    };

    private void Update()
    {
        StartCoroutine(LoadData());
        StartCoroutine(CheckData());
    }
    IEnumerator LoadData()
    {
        string GetDataUrl = "https://a8govb0ic5.execute-api.ap-northeast-2.amazonaws.com/default/Test_01";
        using (UnityWebRequest www = UnityWebRequest.Get(GetDataUrl))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError) //불러오기 실패 시
            {
                apitext.text = "ERROR!!!";
            }
            else
            {
                if (www.isDone)
                {
                    apistring =
                        System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

                    //for test
                    apitext.text = apistring;
                }
            }
        }
    }
    IEnumerator CheckData() {
        if (apistring != null)
        {
            if ((State)int.Parse(apistring) == State.Emergency)
            {
                alarm.callAlarm();
            }
        }
        yield return null;
    }
}
