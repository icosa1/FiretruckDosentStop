﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NaverMap : MonoBehaviour
{
    string url;
    public static string Naver_path = null; 
    static float UPDATE_TIME = 1.0f;
    float updateTime = UPDATE_TIME;
    public ArrowController arrowController;
    public Text apitext;
    public static JSON_DATA Json_data;
    int count = 0;
    public void button()
    {
        SceneManager.LoadScene("Search_xy", LoadSceneMode.Additive);
    }
    void Start()
    {

        StartCoroutine(Navermap());
    }

    void Update()
    {
        if (updateTime >= 0)
        {
            updateTime = updateTime - Time.deltaTime;
        }
        else
        {
            updateTime = UPDATE_TIME;
            StartCoroutine(Navermap());    
        }
    }
  public IEnumerator Navermap()
    {
        if (LocationData.instance.lon2 != 0)
        {
            url = "https://naveropenapi.apigw.ntruss.com/map-direction/v1/driving?start=" + LocationData.instance.lon + "," + LocationData.instance.lat
                   + "&goal=" + LocationData.instance.lon2 + "," + LocationData.instance.lat2;

            UnityWebRequest www = UnityWebRequest.Get(url);
            www.SetRequestHeader("X-NCP-APIGW-API-KEY-ID", "vy3hzhg80c");
            www.SetRequestHeader("X-NCP-APIGW-API-KEY", "NiuCQ8VnrqDB6RZEtz7hu2pmHcNiTP8CWDBOZyIw");

            yield return www.SendWebRequest();
            Json_data = JsonConvert.DeserializeObject<JSON_DATA>(www.downloadHandler.text);

            if (www.isNetworkError || www.isHttpError) //불러오기 실패 시
            {
                apitext.text = www.error;
            }
            if (Json_data.code == 0)
            {
                apitext.text = Json_data.route.traoptimal[0].guide[0].instructions;
                arrowController.ChangeRotation(Json_data.route.traoptimal[0].guide[0].type);

                Naver_path = "&path=color:0xff0000ff|weight:3";
                count = 0;
                foreach (List<string> i in NaverMap.Json_data.route.traoptimal[0].path)
                {
                    Naver_path += "|" + i[1] + "," + i[0];
                    count++;
                    if (count > 700) break; //path의 한계치
                }
            }
            else
            {
                apitext.text = Json_data.message;
                Naver_path = null;
            }
        }
    }
    
    //JSON format
    public class JSON_DATA
    {
        public int code;
        public string message;
        public string currentDateTime;
        public Route route;
    }
    public class Route
    {
        public List<Traoptimal> traoptimal;
        public List<Trafast> trafast;
    }
    public class Trafast
    {
        public List<List<string>> path;
        public Summary summary;
        public List<Section> section;
        public List<Guide> guide;
    }
    public class Traoptimal
    {
        public List<List<string>> path;
        public Summary summary;
        public List<Section> section;
        public List<Guide> guide;
    }
    public class Summary
    {
        public List<List<string>> Bbox;
        public Origin start;
        public Goal goal;
        public string distance;
        public string duration;
        public string etaServiecType;
        public string departureTime;
        public string tollFare;
        public string taxiFare;
        public string fuelPrice;


    }
    public class Origin
    {
        public List<string> location = new List<string>();
    }
    public class Goal
    {
        public List<string> location = new List<string>();
        public string dir;
    }
    public class Section
    {
        public string pointlndex;
        public string pointCount;
        public string distance;
        public string name;
        public string congestion;
        public string speeed;
    }
    public class Guide
    {
        public int pointIndex;
        public int type;
        public string instructions;
        public int distance;
        public int duration;
        public string Get_Path(JSON_DATA json_data, int pointlndex)  // pointlndex에 연동된 path 가져오기
        {
            return json_data.route.traoptimal[0].path[pointIndex][1]+"," + json_data.route.traoptimal[0].path[pointIndex][0];
        }
    }
}
