﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine.Networking;
public class CenterMarkers : MonoBehaviour
{
    /*
     * 이 클래스에서는 소방차(긴급차량)에 관련된 모든 함수가 포함된다. 소방차의 위치 자료, 소방차와의 거리 계산,
     * 구글 맵에게 string를 보내 URL에 추가할 내용을 가진다.
     */    // Start is called before the first frame update
    public MARKER mark_list = new MARKER();
    public static string user_string;
    public static string fire_string;
    static float UPDATE_TIME = 1.5f;
    float updateTime = UPDATE_TIME;

    void Start()
    {  
        update_center();
    }
    void Update()
    {
        if (updateTime >= 0)
        {
            updateTime = updateTime - Time.deltaTime;
        }
        else
        {
            updateTime = UPDATE_TIME;

                update_center();
        }
    }
    void update_center()
    {

            string url3 = "https://bn99wr5gf2.execute-api.ap-northeast-2.amazonaws.com/Control_Center/control";
            HttpWebRequest request3 = (HttpWebRequest)WebRequest.Create(url3);
            request3.Method = ("GET");
            HttpWebResponse response3 = (HttpWebResponse)request3.GetResponse();
            string status = response3.StatusCode.ToString();
            if (status == "OK")
            {
                Stream stream = response3.GetResponseStream();
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                string center_file = reader.ReadToEnd();

                    mark_list = JsonConvert.DeserializeObject<MARKER>(center_file);
                    if (mark_list.General_User != null)
                    {
                        user_string = null;
                        foreach (USER i in mark_list.General_User)
                        {
                            if (i.Latitude != 0 && i.D_Lat != 0)
                            {
                            user_string += "&markers=color:green%7Clabel:C%7C" + i.Latitude + "," + i.Longitude;
                            user_string += "&markers=color:red%7Clabel:C%7C" + i.D_Lat + "," + i.D_Lon;
                            StartCoroutine(Navermap(i.Longitude, i.Latitude, i.D_Lon, i.D_Lat));
                            }
                        }
                    }
                    if (mark_list.Fire_Truck_User != null)
                    {
                        fire_string = null;
                        foreach (FIRE i in mark_list.Fire_Truck_User)
                        {
                            if (i.Latitude != 0 )
                                fire_string += "&markers=icon:https://i.imgur.com/WovgtJl.png%7C" + i.Latitude + "," + i.Longitude;

                        }
                    }
            }
    }

    public IEnumerator Navermap(double lon, double lat, double lon2, double lat2)
    {
        
        string naverurl;
        string Naver_path = "";
        int count;

        naverurl = "https://naveropenapi.apigw.ntruss.com/map-direction/v1/driving?start=" + lon + "," + lat
            + "&goal=" + lon2 + "," + lat2;

        UnityWebRequest www = UnityWebRequest.Get(naverurl);
        www.SetRequestHeader("X-NCP-APIGW-API-KEY-ID", "vy3hzhg80c");
        www.SetRequestHeader("X-NCP-APIGW-API-KEY", "NiuCQ8VnrqDB6RZEtz7hu2pmHcNiTP8CWDBOZyIw");

        yield return www.SendWebRequest();

        JSON_DATA Json_data = JsonConvert.DeserializeObject<JSON_DATA>(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError) //불러오기 실패 시
        {
            Debug.Log("CenterMap error: " + www.error);
        }

        if (Json_data != null)
        {
            Naver_path = "&path=color:0xff0000ff|weight:3";
            count = 0;
            foreach (List<string> i in Json_data.route.traoptimal[0].path)
            {
                Naver_path += "|" + i[1] + "," + i[0];
                count++;
                if (count > 700) break; //path의 한계치
            }
        }
        user_string += Naver_path;
    }

    public class MARKER //Json 역직렬화를 위한 데이터.
    {
        public List<USER> General_User;
        public List<FIRE> Fire_Truck_User;
    }
    public class USER
    {
        public string User_ID;
        public double Longitude;
        public double Latitude;
        public double D_Lon;
        public double D_Lat;
    }
    public class FIRE
    {
        public string User_ID;
        public double Longitude;
        public double Latitude;
    }

    //naverdata
    public class JSON_DATA
    {
        public string code;
        public string message;
        public string currentDateTime;
        public Route route;
    }
    public class Route
    {
        public List<Traoptimal> traoptimal;
        public List<Trafast> trafast;
    }
    public class Trafast
    {
        public List<List<string>> path;
        public Summary summary;
        public List<Section> section;
        public List<Guide> guide;
    }
    public class Traoptimal
    {
        public List<List<string>> path;
        public Summary summary;
        public List<Section> section;
        public List<Guide> guide;
    }
    public class Summary
    {
        public List<List<string>> Bbox;
        public Origin start;
        public Goal goal;
        public string distance;
        public string duration;
        public string etaServiecType;
        public string departureTime;
        public string tollFare;
        public string taxiFare;
        public string fuelPrice;


    }
    public class Origin
    {
        public List<string> location = new List<string>();
    }
    public class Goal
    {
        public List<string> location = new List<string>();
        public string dir;
    }
    public class Section
    {
        public string pointlndex;
        public string pointCount;
        public string distance;
        public string name;
        public string congestion;
        public string speeed;
    }
    public class Guide
    {
        public int pointIndex;
        public int type;
        public string instructions;
        public int distance;
        public int duration;
        public string Get_Path(JSON_DATA json_data, int pointlndex)  // pointlndex에 연동된 path 가져오기
        {
            return json_data.route.traoptimal[0].path[pointIndex][1] + "," + json_data.route.traoptimal[0].path[pointIndex][0];
        }
    }
}
