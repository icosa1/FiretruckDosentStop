﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BigGoogleMap : MonoBehaviour
{
    public GameObject Bigmap_panel;
    public RawImage BigmapRawImage;
    string url,temp;
    public int zoom;
    public int mapWidth;
    public int mapHeight;
    public enum mapType { roadmap, satellite, hybrid, terrain }
    public mapType mapSelected;
    public int scale;
    public static int count = 0;
    public static bool ison = false; 

    // Start is called before the first frame update
    public void Start()
    {
        Bigmap_panel.SetActive(false);
    }
    public void call_BigMap()
    {
        count++;
        if (count == 2)
        {
            ison = true;
            count = 0;
            StartCoroutine(BigMap());
            Bigmap_panel.SetActive(true);
        }
    }
    public void Delete_BigMap()
    {
        count++;
        if (count == 2)
        {
            ison = false;
            count = 0;
            Bigmap_panel.SetActive(false);
        }
    }
    public IEnumerator BigMap()
    {
        zoom = 14;
        mapWidth = 640;
        mapHeight = 360;
        scale = 4;

        url = "https://maps.googleapis.com/maps/api/staticmap?center=" + LocationData.instance.lat + "," + LocationData.instance.lon +
            "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
            + "&maptype=" + mapSelected + "&path=" + NaverMap.Naver_path +
            "&key=AIzaSyDout7WR-ZxWz82Ualbi8oPQyvfq3Y_Pg0" +
            "&markers=color:green%7Clabel:C%7C" + LocationData.instance.lat + "," + LocationData.instance.lon +
            "&markers=color:red%7Clabel:C%7C" + LocationData.instance.lat2 + "," + LocationData.instance.lon2 +
            "&markers=color:blue%7Clabel:C%7C" + LocationData.instance.lat3 + "," + LocationData.instance.lon3;
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        BigmapRawImage.texture = DownloadHandlerTexture.GetContent(www);
        //BigmapRawImage.SetNativeSize();
    }
}
