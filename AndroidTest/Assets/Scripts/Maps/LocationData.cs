﻿using UnityEngine;

public class LocationData : MonoBehaviour
{
    //public double lat = 37.281221164334774; //GPS 신호 - Current Default: 아주대 정문
    //public double lon = 127.04353604757667;
    public double lat = 37.280767873476165; //GPS 신호 - Current Default: 아주대 정문
    public double lon = 127.04359580770881; 
    public double lat2 = 0; //목적지 - Destni Default: 아주대 삼거리 가는 쪽 상가
    public double lon2 = 0;
    public double lat3 = 37.28297366204902; //출발지 - Start Default: 아주대학교
    public double lon3 = 127.04471858407705;
    public double current_lat =0; //GPS를 업데이트 받을 때 lat,lon에 영향을 주지 않기 위해 신설
    public double current_lon =0;
    public double post_lat = 37.2812206149424;
    public double post_lon = 127.043608347635;
     
    public static LocationData instance = null;

    private void Awake()
    {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else{
            if (instance != this) {
                Destroy(this.gameObject);
            }
        }
    }
}
