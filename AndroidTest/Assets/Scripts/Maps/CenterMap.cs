﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class CenterMap : MonoBehaviour
{
    public RawImage mapRawImage;

    string url;
    string path;

    public int zoom;
    public int mapWidth;
    public int mapHeight;

    public enum mapType { roadmap, satellite, hybrid, terrain }
    public mapType mapSelected;
    public int scale;
   
    static float UPDATE_TIME = 2f;
    float updateTime = UPDATE_TIME;
    private void Awake()
    {
        mapWidth = 640;
        mapHeight = 360;
        zoom = 15;
        scale = 4;
        mapSelected = mapType.roadmap;
    }
    IEnumerator Map()
    {
        url = "https://maps.googleapis.com/maps/api/staticmap?center=37.281221164334774,127.04353604757667" +
            "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
            + "&maptype=" + mapSelected +
            "&key=AIzaSyDout7WR-ZxWz82Ualbi8oPQyvfq3Y_Pg0" + CenterMarkers.fire_string
            + CenterMarkers.user_string;
        UnityWebRequest www = UnityWebRequestTexture.GetTexture (url);
        yield return www.SendWebRequest();
        mapRawImage.texture = DownloadHandlerTexture.GetContent(www);
    }
    
    void Start()
    {
        mapRawImage = gameObject.GetComponent<RawImage>();
        StartCoroutine(Map());
    }

    private void Update()
    {
        if (updateTime >= 0)
        {
            updateTime = updateTime - Time.deltaTime;
        }
        else
        {
            updateTime = UPDATE_TIME;

            StartCoroutine(Map());
        }
    }
    
}
