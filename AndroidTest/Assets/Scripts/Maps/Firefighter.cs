﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class Firefighter : MonoBehaviour
{
    /*
     * 이 클래스에서는 소방차(긴급차량)에 관련된 모든 함수가 포함된다. 소방차의 위치 자료, 소방차와의 거리 계산,
     * 구글 맵에게 string를 보내 URL에 추가할 내용을 가진다.
     */    // Start is called before the first frame update
    public FIREFIGHTER fire_list = new FIREFIGHTER();
    public static string fire_string;
    public double fire_lat;
    public double fire_lon;
    static float UPDATE_TIME = 3f;
    float updateTime = UPDATE_TIME;
    public static string path = @"file:\..\Assets\Scripts\Maps\firecar_test.txt";
    public static string path_center = @"file:\..\Assets\Scripts\Maps\center_demo.txt";
    StreamReader fire_rs = new StreamReader(path);
    StreamReader center_demo = new StreamReader(path_center);
    public RawImage direction_fire;
    public GameObject direction_panel;
    public Alarm alarm;
    void Start()
    {
        direction_panel.SetActive(false);
        //query_emergency(); //소방차 서버 좌표 업데이트
        update_center(); //사용자가 자신의 위치를 업데이트하고, 근처 소방차의 주소를 받아온다.
        //demo();
    }
    void Update()
    {
        if (updateTime >= 0)
        {
            updateTime = updateTime - Time.deltaTime;
        }
        else
        {
            updateTime = UPDATE_TIME;

            if (LocationData.instance.lat != 0f && LocationData.instance.lon != 0f)
            {
                //query_emergency(); //소방차 서버 좌표 업데이트
                update_center();
                //demo(); //데모버전 -> 사용시 update_center 주석처리하기.
            }
        }
    }
    void update_center()
    {
        string user_ID = "Normal_1";
        string url3 = string.Format(url3 = "https://fmpvdxstoa.execute-api.ap-northeast-2.amazonaws.com/update_and_alert/update?User_ID={0}&Latitude={1}&Longitude={2}",user_ID, LocationData.instance.lat, LocationData.instance.lon);
        HttpWebRequest request3 = (HttpWebRequest)WebRequest.Create(url3);
        request3.Method = ("PATCH");
        HttpWebResponse response3 = (HttpWebResponse)request3.GetResponse();
        string status = response3.StatusCode.ToString();
        if (status == "OK")
        {
            Stream stream = response3.GetResponseStream();
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            string center_file = reader.ReadToEnd();
            if (string.Equals(center_file, "\"Normal\""))
            {
                fire_string = null;
            }
            else
            {
                alarm.callAlarm(); // 알람 울리기

                fire_list = JsonConvert.DeserializeObject<FIREFIGHTER>(center_file);
                if (fire_list.items != null)
                {
                    fire_string = null;
                    foreach (FIRE i in fire_list.items)
                    {
                       
                            double stand, rota;
                            (stand, rota) = radian(i.Latitude, i.Longitude);
                            direct(stand, rota);
                        
                        fire_string += "&markers=icon:https://i.imgur.com/WovgtJl.png%7C" + i.Latitude + "," + i.Longitude;
                    }
                }
            }
        }
        response3.Close();

    }

    void query_emergency()
    {
        string textValue = fire_rs.ReadLine();
        
        if (textValue.Length > 0 && !fire_rs.EndOfStream)
        {
            string[] text = textValue.Split(',');
            fire_lat = Double.Parse(text[0]);
            fire_lon = Double.Parse(text[1]);
            string url2 = string.Format(url2 = "https://8qwhyk09b0.execute-api.ap-northeast-2.amazonaws.com/update_fire_truck_user_location/update?User_ID=test&Latitude={0}&Longitude={1}", fire_lat, fire_lon);
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url2);
            request1.Method = ("PATCH");
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        else
        {
            fire_rs.Close();
            fire_rs = new StreamReader(path);
        }
    }
          
    
    void demo()
    {
        string textValue = center_demo.ReadLine();

        if (textValue.Length > 0 && !center_demo.EndOfStream)
        {
            string[] text = textValue.Split(',');
            double center_lat = Double.Parse(text[0]);
            double center_lon = Double.Parse(text[1]);
            LocationData.instance.lat = center_lat;
            LocationData.instance.lon = center_lon;
            string url3 = string.Format(url3 = "https://fmpvdxstoa.execute-api.ap-northeast-2.amazonaws.com/update_and_alert/update?User_ID=test&Latitude={0}&Longitude={1}", center_lat, center_lon);
            HttpWebRequest request3 = (HttpWebRequest)WebRequest.Create(url3);
            request3.Method = ("PATCH");
            HttpWebResponse response3 = (HttpWebResponse)request3.GetResponse();
            string status = response3.StatusCode.ToString();
            if (status == "OK")
            {
                Stream stream = response3.GetResponseStream();
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                string center_file = reader.ReadToEnd();
                if (string.Equals(center_file, "\"Normal\""))
                {
                    fire_string = null;
                }
                else
                {
                    alarm.callAlarm(); // 알람 울리기

                    fire_list = JsonConvert.DeserializeObject<FIREFIGHTER>(center_file);
                    if (fire_list.items != null)
                    {
                        fire_string = null;
                        foreach (FIRE i in fire_list.items)
                        {
                            
                            fire_string += "&markers=icon:https://i.imgur.com/WovgtJl.png%7C" + i.Latitude + "," + i.Longitude;
                        }
                    }
                }
            }
        }
        else
        {
            center_demo.Close();
            center_demo = new StreamReader(path_center);
        }
    }
    public (double,double) radian(double fire_lat,double fire_lon)
    {
        double theta1 = LocationData.instance.post_lat * Math.PI / 180;
        double lambda1 = LocationData.instance.post_lon * Math.PI / 180;
        double theta2 = LocationData.instance.lat * Math.PI / 180;
        double lambda2 = LocationData.instance.lon * Math.PI / 180;
        double fire_theta = fire_lat * Math.PI / 180;
        double fire_lambda = fire_lon * Math.PI / 180;
        double y = Math.Sin(lambda2 - lambda1) * Math.Cos(theta2);
        double x = Math.Cos(theta1) * Math.Sin(theta2) - Math.Sin(theta1) * Math.Cos(theta2) * Math.Cos(lambda2 - lambda1);
        double radian = Math.Atan2(y, x);
        double bearing = (radian * 180 / Math.PI + 360) % 360;
        double fire_y = Math.Sin(fire_lambda - lambda2) * Math.Cos(fire_theta);
        double fire_x = Math.Cos(theta2) * Math.Sin(fire_theta) - Math.Sin(theta2) * Math.Cos(fire_theta) * Math.Cos(fire_lambda - lambda2);
        double fire_radian = Math.Atan2(fire_y, fire_x);
        double fire_bearing = (fire_radian * 180 / Math.PI + 360) % 360;
        return (bearing,fire_bearing);
        
    }
    public void direct(double standard, double rotation)
    {
        float Heigth = 480;
        float Width = 930;
        double x = 0;
        double y = 0;

        double bearing = rotation-standard;
        if (bearing < 0)
            bearing += 360;
        Debug.Log(bearing);
        double radian = bearing * Math.PI / 180;
        if (((315 < bearing && bearing <= 360) || (bearing >= 0 && bearing <= 45)))
        {
            //y는 그대로 x만 변경
            x = Heigth * Math.Tan(radian);
            y = Heigth;
            direct_check(x, y);
        }
        if ((135 >= bearing && bearing > 45))
        {
            //x는 그대로 y 만 변경
            x = Width;  
            if ((90 >= bearing && bearing > 45))
                y = Width * Math.Tan((Math.PI / 2 - radian));
            else
                y = Width * Math.Tan((Math.PI/2)- radian);
            direct_check(x, y);
        }
        if ((225 >= bearing && bearing > 135))
        {
            //y는 그대로 x만 변경
            x = -Heigth * Math.Tan(radian);
            y = -Heigth;
            direct_check(x, y);
        }
        if ((315 >= bearing && bearing > 225))
        {
            //x는 그대로 y 만 변경
            x = -Width;
            if ((270 >= bearing && bearing > 225))
                y = Width * Math.Tan((radian-Math.PI*3/2));
            else
                y = Width * Math.Tan((radian-Math.PI*3/2));
            direct_check(x, y);
        }     
    }
    public void direct_check(double x,double y)
    {
        Vector3 position = direction_panel.transform.localPosition;
        direction_panel.SetActive(true);
        if (x > 930)
            x = 930;
        if (x < -930)
            x = -930;
        if (y > 480)
            y = 480;
        if (y < -480)
            y = -480;
        position.x = (float)x;
        position.y = (float)y;
        direction_panel.transform.localPosition = position;
    }
    public class FIREFIGHTER //Json 역직렬화를 위한 데이터.
    {
       public List<FIRE> items;
    }
    public class FIRE
    {
        public string User_ID;
        public double Longitude;
        public double Latitude;
        public double meter;
    }
}
