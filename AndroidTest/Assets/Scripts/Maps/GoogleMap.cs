﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GoogleMap : MonoBehaviour
{
    public static LocationInfo lo;
    public RawImage mapRawImage;

    string url;
    string path;

    public static bool gpsStarted = false;
    private static WaitForSeconds second;

    public int zoom;
    public int mapWidth;
    public int mapHeight;

    public enum mapType { roadmap, satellite, hybrid, terrain }
    public mapType mapSelected;
    public int scale;
   
    static float UPDATE_TIME = 3f;
    float updateTime = UPDATE_TIME;


    IEnumerator Map()
    {
        url = "https://maps.googleapis.com/maps/api/staticmap?center=" + LocationData.instance.lat + "," + LocationData.instance.lon +
            "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
            + "&maptype=" + mapSelected + NaverMap.Naver_path +
            "&key=AIzaSyDout7WR-ZxWz82Ualbi8oPQyvfq3Y_Pg0" + Firefighter.fire_string +
            "&markers=color:green%7Clabel:C%7C" + LocationData.instance.lat + "," + LocationData.instance.lon;
        if (LocationData.instance.lon2 != 0)
        {
           url+= "&markers=color:red%7Clabel:C%7C" + LocationData.instance.lat2 + "," + LocationData.instance.lon2;
        }
        UnityWebRequest www = UnityWebRequestTexture.GetTexture (url);
        yield return www.SendWebRequest();
        mapRawImage.texture = DownloadHandlerTexture.GetContent(www);
        
    }

    

    private void Awake()
    {
        second = new WaitForSeconds(3.0f);
        mapWidth = 300;
        mapHeight = 300;
        zoom = 16;
        mapSelected = mapType.roadmap;
    }
    void Start()
    {
        StartCoroutine(StartGPS());
        mapRawImage = gameObject.GetComponent<RawImage>();
        StartCoroutine(Map());
    }

    private void Update()
    {
        if (updateTime >= 0)
        {
            updateTime = updateTime - Time.deltaTime;
        }
        else
        {
            updateTime = UPDATE_TIME;

            if (LocationData.instance.lat != 0f && LocationData.instance.lon != 0f)
            {
                StartCoroutine(GameObject.Find("map").GetComponent<NaverMap>().Navermap());
                StartCoroutine(Map());

            }
        }
    }


    IEnumerator StartGPS()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation)) {
            Permission.RequestUserPermission(Permission.FineLocation);

            while (!Permission.HasUserAuthorizedPermission(Permission.FineLocation)) {
                yield return null;
            }
        }

        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        Input.location.Start(5f);

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            gpsStarted = true;
            // Access granted and location value could be retrieved
            while (gpsStarted)
            {
                lo = Input.location.lastData;
                LocationData.instance.post_lat = LocationData.instance.lat;
                LocationData.instance.post_lon = LocationData.instance.lon;
                LocationData.instance.lat = lo.latitude;
                LocationData.instance.lon = lo.longitude;
                LocationData.instance.current_lat = lo.latitude;
                LocationData.instance.current_lon = lo.longitude;

                yield return second;
            }
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }
    
    public void exitGame()
    {
        Application.Quit();
    }        
}
