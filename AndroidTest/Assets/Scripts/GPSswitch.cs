﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GPSswitch : MonoBehaviour
{
    public Text gpstext;

    public void GPSchange()
    {
        if (GoogleMap.gpsStarted == true) {
            GoogleMap.gpsStarted = false;
            gpstext.text = "GPS OFF";
        }
        else if (GoogleMap.gpsStarted == false)
        {
            GoogleMap.gpsStarted = true;
            gpstext.text = "GPS ON";
        }
    }
}
