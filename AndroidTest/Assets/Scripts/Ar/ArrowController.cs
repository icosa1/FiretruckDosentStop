﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ArrowController : MonoBehaviour
{
    public GameObject arrow; //normal arrow
    public GameObject arrow2; //U-turn

    public void ChangeRotation(int direction)
    {
        if (direction == 3 || direction == 5) // right
        {
            arrow2.SetActive(false);
            if (arrow.activeSelf == false) { arrow.SetActive(true); }
            arrow.transform.rotation = Quaternion.Euler(arrow.transform.rotation.x, -13f, arrow.transform.rotation.z);
        }
        else if (direction == 2 || direction == 4) //left
        {
            arrow2.SetActive(false);
            if (arrow.activeSelf == false) { arrow.SetActive(true); }
            arrow.transform.rotation = Quaternion.Euler(arrow.transform.rotation.x, 167f, arrow.transform.rotation.z);
        }
        else if (direction == 6) // U-turn
        {
            arrow.SetActive(false);
            if (arrow2.activeSelf == false) { arrow2.SetActive(true); }
            arrow2.transform.rotation = Quaternion.Euler(arrow.transform.rotation.x, 167f, arrow.transform.rotation.z);
        }
        else
        {
            if (arrow.activeSelf == true) {
                arrow.SetActive(false);
                arrow2.SetActive(false);
            }
        }

        if (BigGoogleMap.ison)
        {
            arrow.SetActive(false);
            arrow2.SetActive(false);

        }
    }
}
