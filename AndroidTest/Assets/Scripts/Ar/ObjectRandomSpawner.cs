﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ObjectRandomSpawner : MonoBehaviour
{
    public GameObject spawnTarget;
    public ARPlaneManager arPlaneManager;
    private Dictionary<TrackableId, GameObject> spawnedObjects;

    private void Start()
    {
        arPlaneManager.planesChanged += SpawnObjects;
    }

    private void SpawnObjects(ARPlanesChangedEventArgs args) {
        List<ARPlane> addedPlanes = args.added;

        // make new objects when a plane is added
        if (addedPlanes.Count > 0) {
            foreach (ARPlane plane in addedPlanes) {
                Vector3 spawnPosition = plane.center
                    + Random.Range(0, plane.boundary[0].x) * plane.transform.right
                    + Random.Range(0, plane.boundary[0].y) * plane.transform.forward;

                GameObject instance = Instantiate(spawnTarget, spawnPosition, plane.transform.rotation);
                spawnedObjects.Add(plane.trackableId, instance);
            }
        }

        List<ARPlane> removedPlanes = args.removed;

        // delete objects when the planes are removed
        if (removedPlanes.Count > 0) {
            foreach (ARPlane plane in removedPlanes) {
                GameObject destroyTarget = spawnedObjects[plane.trackableId];
                Destroy(destroyTarget);
            }
        }
    }
}
