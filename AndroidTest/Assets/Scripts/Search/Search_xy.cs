﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;

public class Search_xy : MonoBehaviour //목적 버튼을 클릭시 네이버에 쿼리를 보내서 JSON Data받고 그 데이터를 보관하고 있는다.
{
    public ScrollRect rect;
    private Touch touch;
    public InputField Search_bar;
    public Text Search_bar_placeholder;
    public Text GPS_button_text;
    public JSON_GEOCODE json_geocode = new JSON_GEOCODE();
    
    public int count_start = -1; //선택안됨: -1 , 선택됨: x 번째 선택지의 x값
    public int count_dest = -1;

    public Text[] answer_Text;
    public Text[] where; // 0 : 출발지 , 1: 목적지
    public GameObject[] answer_Panel; //연동한 패널 
    
    public static List<string> road = new List<string>();
    public static List<double> lat_value = new List<double>();
    public static List<double> lon_value = new List<double>();
    public double temp_start_lat = 0;
    public double temp_start_lon = 0;
    public double temp_dest_lat = 0;
    public double temp_dest_lon = 0;
    public void back_button() //뒤로가기 버튼을 누르면.
    {
        if(count_start>=0 && count_dest>=0)
        {   
            LocationData.instance.lat3 = temp_start_lat;
            LocationData.instance.lat2 = temp_dest_lat; 
            LocationData.instance.lon3 = temp_start_lon;
            LocationData.instance.lon2 = temp_dest_lon;
            SceneManager.UnloadSceneAsync("Search_xy");
        }
        else
        {
            SceneManager.UnloadSceneAsync("Search_xy");
        }
    }
    public void GPS_button() //GPS 버튼을 누르면
    {
        if (count_start == -1)
        {      
            if(LocationData.instance==null) //GPS가 작동하지 않은 경우.
            {
                Search_bar_placeholder.text = "GPS 신호가 작동하지 않습니다.";
                return;
            }
            else 
            {
                if (LocationData.instance.current_lat != 0 && LocationData.instance.current_lon != 0)
                {
                    temp_start_lat = LocationData.instance.current_lat;
                    temp_start_lon = LocationData.instance.current_lon;
                    count_start = 10;
                    where[0].text = " 현재 내위치 - GPS ";
                    GPS_button_text.text = "/GPS";
                }
                else
                {
                    Search_bar_placeholder.text = "GPS 데이터가 없습니다.";
                    return;
                }
            }
        }
        else
        {
            if (count_start == 10)
            {
                count_start = -1;
                where[0].text = null;
                GPS_button_text.text = "GPS";
            }
        }
    }
    public void Start()
    {        
        count_start = -1;
        count_dest = -1;      
    }
    void init_naver()
    {
        for (int i = 0; i < 10; i++)
            rect.content.GetChild(i).gameObject.SetActive(false);
        rect.verticalScrollbar.value = 1;
        road.Clear();
        lat_value.Clear();
        lon_value.Clear();     
        Search_bar_placeholder.text = "도로명 주소, 지번으로 검색";
    }
    public void Naver_Geocode(Text search)
    {
        init_naver();
        string text;
        int index = 0;
        string url = string.Format(url = "https://naveropenapi.apigw.ntruss.com/map-geocode/v2/geocode?query={0}",search.text);
        Search_bar.text = null;
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Headers.Add("X-NCP-APIGW-API-KEY-ID", "vy3hzhg80c"); // 클라이언트 아이디
        request.Headers.Add("X-NCP-APIGW-API-KEY", "NiuCQ8VnrqDB6RZEtz7hu2pmHcNiTP8CWDBOZyIw"); //클라이언트 key
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        string status = response.StatusCode.ToString();
        if (status == "OK")
        {
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            text = reader.ReadToEnd();
            json_geocode = JsonConvert.DeserializeObject<JSON_GEOCODE>(text);
            if (json_geocode.status != "OK")
            {
                Search_bar_placeholder.text = "오류! 다시 시도해주세요.";
                return;
            }
            if(json_geocode.meta.count==0)
            {
                Search_bar_placeholder.text = "정보가 없습니다. 도로명 주소, 지번으로 검색해주세요.";
                return;
            }
            foreach (Addresses i in json_geocode.addresses)
            {
                if (index > 9)
                    break;
                answer_Text[index].text = i.roadAddress;
                if (answer_Text[index].text == where[0].text || answer_Text[index].text == where[1].text)
                {
                    answer_Text[index].fontSize = 60;
                    answer_Text[index].text = "<b>" + i.roadAddress+ "</b>";
                }           
                rect.content.GetChild(index).gameObject.SetActive(true);
                road.Add(i.roadAddress);
                lat_value.Add(i.y);
                lon_value.Add(i.x);
                index++;
            }
        }
        else
        {
            Debug.Log("NAVER_qurl_ERROR!");
            return;
        }
    }
    
    void Update()
    {
        
        if (Input.touchCount > 0) // 컴퓨터로 유니티 테스트시 사용 Input.GetMouseButtonUp(0) Input.GetMouseButtonUp(0)
        {
            
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began || EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                if (EventSystem.current.currentSelectedGameObject == null)
                    return;

                switch (EventSystem.current.currentSelectedGameObject.name)
                {
                    case "Button0":
                        ChoiceManage(0);
                        break;
                    case "Button1":
                        ChoiceManage(1);
                        break;
                    case "Button2":
                        ChoiceManage(2);
                        break;
                    case "Button3":
                        ChoiceManage(3);
                        break;
                    case "Button4":
                        ChoiceManage(4);
                        break;
                    case "Button5":
                        ChoiceManage(5);
                        break;
                    case "Button6":
                        ChoiceManage(6);
                        break;
                    case "Button7":
                        ChoiceManage(7);
                        break;
                    case "Button8":
                        ChoiceManage(8);
                        break;
                    case "Button9":
                        ChoiceManage(9);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    void ChoiceManage(int index)
    {
        Search_bar_placeholder.text = "똑같은 주소 클릭시 제거됩니다.";
        if (count_start == -1) //출발지가 없는 경우
        {
            count_start = index;
            where[0].text = road[index];
            temp_start_lat = lat_value[index];
            temp_start_lon = lon_value[index];
            answer_Text[index].fontSize = 60;
            answer_Text[index].text = "<b>" + road[index] + "</b>";
            if (where[0].text != null && where[0].text == where[1].text) //둘다 같은 목적지를 가지는 경우
            {
                count_start = -1;
                count_dest = -1;
                where[0].text = null;
                where[1].text = null;
                answer_Text[index].fontSize = 50;
                answer_Text[index].text = road[index];

            }
        }
        else //출발지가 있는 경우
        {
            if (where[0].text == road[index]) //출발지와 같은 제목을 다시 한번 선택한 경우라면
            {
                count_start = -1;
                where[0].text = null;
                answer_Text[index].fontSize = 50;
                answer_Text[index].text = road[index];
            }
            else //출발지가 있고, 출발지와 중복된 제목을 선택한게 아니라면
            {
                if (count_dest == -1) //목적지가 없는 경우
                {
                    count_dest = index;
                    where[1].text = road[index];
                    temp_dest_lat = lat_value[index];
                    temp_dest_lon = lon_value[index];
                    answer_Text[index].fontSize = 60;
                    answer_Text[index].text = "<b>" + road[index] + "</b>";
                }
                else
                {
                    if (where[1].text == road[index]) //목적지와 같은 제목을 다시 한번 선택한 경우(취소)
                    {
                        count_dest = -1;
                        where[1].text = null;
                        answer_Text[index].fontSize = 50;
                        answer_Text[index].text = road[index];
                    }
                }
            }
        }
        
    }
    // 이곳은 Json 역직렬화를 위한 Class 생성 공간입니다.
    public class JSON_GEOCODE
    {
        public string status;
        public Meta meta;
        public List<Addresses> addresses;
    }
    public class Meta
    {
        public int totalCount;
        public int page;
        public int count;
    }
    public class Addresses
    {
        public string roadAddress;
        public string jibunAddress;
        public string englishAddress;
        public List<AddressElements> addressElements;
        public string longName;
        public string shortName;
        public string code;
        public double x;
        public double y;
        public double distance;
    }
    public class AddressElements
    {
        public List<string> types;
    }
}
