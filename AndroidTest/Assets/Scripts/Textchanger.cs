﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Textchanger : MonoBehaviour
{
    public Text notice;

    public void textchange(string a)
    {
        StartCoroutine(TextChange(a));
    }

    IEnumerator TextChange(string sentence) {
        notice.text = sentence;
        yield return new WaitForSeconds(3.0f);
        notice.text = "";
    }
}
