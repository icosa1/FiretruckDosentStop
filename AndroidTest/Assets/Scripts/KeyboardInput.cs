﻿using UnityEngine;
using UnityEngine.UI;

public class KeyboardInput : MonoBehaviour
{
    public Text keyboardtext;
    private TouchScreenKeyboard keyboard;

    // Opens native keyboard
    public void OpenKeyboard()
    {
        keyboard = TouchScreenKeyboard.Open(keyboardtext.text, TouchScreenKeyboardType.Default);
    }
    void Update()
    {
        if (keyboard != null && keyboard.status == TouchScreenKeyboard.Status.Done)
        {
            keyboardtext.text = keyboard.text;
        }
    }
}
