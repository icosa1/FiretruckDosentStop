﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Alarm : MonoBehaviour
{
    public Image img;
    public AudioSource alarmaudio;

    bool ison = false;
    public void callAlarm() {
        if(!ison) StartCoroutine(CallAlarm());
    }
    IEnumerator CallAlarm() {
        ison = true;
        img.enabled = true;
        alarmaudio.Play();
        yield return new WaitForSeconds(0.5f);
        img.enabled = false;
        yield return new WaitForSeconds(0.5f);
        img.enabled = true;
        yield return new WaitForSeconds(0.5f);
        img.enabled = false;
        yield return new WaitForSeconds(0.5f);
        img.enabled = true;
        yield return new WaitForSeconds(0.5f);
        img.enabled = false;
        yield return new WaitForSeconds(0.5f);
        img.enabled = true;
        yield return new WaitForSeconds(0.5f);
        img.enabled = false;
        yield return new WaitForSeconds(0.5f);
        img.enabled = true;
        yield return new WaitForSeconds(0.5f);
        img.enabled = false;
        yield return new WaitForSeconds(0.5f);
        img.enabled = true;
        yield return new WaitForSeconds(0.5f);
        img.enabled = false;
        yield return new WaitForSeconds(0.5f);
        ison = false;
    }
}
